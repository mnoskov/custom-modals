<?php

return [
    'title' => 'Модальные окна',

    'show_in_docs' => [0],

    'placement' => 'tv',

    'templates' => [
        'owner' => '
            [+wrap+]
            <link rel="stylesheet" type="text/css" href="assets/templates/default/css/custommodals.css">
        ',
    ],
];

