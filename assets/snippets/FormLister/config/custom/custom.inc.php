<?php

if (!defined('IN_PARSER_MODE')) {
    return [];
}

$data = $modx->runSnippet('PageBuilder', [
    'docid' => 0,
    'container' => 'modals',
    'renderTo' => 'array',
]);

$data = $data[0][0];

if (empty($data['fields'])) {
    die();
}

$rules = [];
$body = '';

foreach ($data['fields'] as $i => $field) {
    $rule = [];
    $key = 'field_' . $i;

    switch ($field['type']) {
        case 'phone': {
            $rule['matches'] = [
                'params'  => '/^\+?[78]\s?\(\d{3}\)\s?\d{3}-\d\d-\d\d$/',
                'message' => 'Формат телефона неверный',
            ];
            $key = 'phone';
            break;
        }

        case 'email': {
            $rule['email'] = 'Формат адреса неверный';
            $key = 'email';
            break;
        }
    }

    if (!empty($field['required'][0])) {
        $rule['required'] = 'Обязательное поле';
        $rules[$key] = $rule;
    } else {
        $rules['!' . $key] = $rule;
    }


    $body .= '<tr><td>' . $field['caption'] . ':&nbsp;</td><td>[+' . $key . '.value+]</td></tr>';
}

$rules['agree'] = [
    'required' => 'Отметьте согласие',
];

return [
    'debug' => 1,
    'rules'     => $rules,
    'subject'   => $data['letter_subject'] . '!',
    'reportTpl' => '@CODE:<b>' . $data['letter_subject'] . '!</b><table>' . $body . '</table>',
];
